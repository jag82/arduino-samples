# addressable leds

There are many kinds of addressable LED strips. I'll have to check model numbers, but shortly explained: the main kind I used for Starlight was addressable in sets of 3. There is a more expensive variety where each individual light can be changed (addressed).

Long strips (e.g. 5m) require a good voltage/amperage or the last lights will be dim.

The FastLED library is a must. Use it!

Try FirstLight.ino to test lightstrips. You'll need to know the model number, number of lights, and you can guess the light order (e.g. RGB, GRB, etc.).

