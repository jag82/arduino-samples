#include <FastLED.h>

#define NUM_LEDS 100
#define DATA_PIN 6

CRGB leds[NUM_LEDS];

int DIAL = A0;

// int LEFT_PIN = 13;
// int RIGHT_PIN = 12;

int index = 0;
float speed = 0.1df;
// int MAX_BRIGHT = 50;
// int brightness = 255;
// int fadeRate = 10;

void setup() {
 delay(3000);

	Serial.begin(9600);

	pinMode(DIAL, INPUT);

  FastLED.addLeds<WS2811, DATA_PIN, RGB>(leds, NUM_LEDS);
  // FastLED.addLeds<WS2812B, DATA_PIN, BRG>(leds, NUM_LEDS);

  // pinMode(LEFT_PIN, INPUT);
  // pinMode(RIGHT_PIN, INPUT);
}


void loop() {
  // 0 - 1023
	int dial = analogRead(DIAL);

  // 0.00 - 1.00
	float fraction = dial/1023.00;

  FastLED.clear();

  index = fraction * NUM_LEDS;
  if (index == NUM_LEDS) {
    index--;
  }
  leds[index] = CRGB::White;

  FastLED.show();

  // loopFastLED();
  // int left = digitalRead(LEFT_PIN);
  // int right = digitalRead(RIGHT_PIN);

  // if (left && !right) {
  //   if (index > 0) {
  //     index--;
  //   }
  // }

  // if (right && !left) {
  //   if (index < NUM_LEDS - 1) {
  //     index++;
  //   }
  // }

  // brightness -= fadeRate;
  // if (brightness <= 0) {
  //   fadeRate *= -1;
  //   brightness = 0;
  // }
  // if (brightness >= MAX_BRIGHT) {
  //   fadeRate *= -1;
  //   brightness = MAX_BRIGHT;
  // }


  // FastLED.clear();
  // leds[1] = CRGB::White;
  // leds[index] = CRGB::White;

  // for (int i = 0; i < NUM_LEDS; i++) {
  //   leds[i] = CRGB::Blue;
  //   leds[i].fadeLightBy(254);
  // }

  // leds[index] = CRGB::HotPink;
  // if (brightness < 255) {
  //   leds[index].fadeLightBy(255 - brightness);
  // }
  // FastLED.show();

  // index++;
  // if (index >= NUM_LEDS) {
  //   index = -1;
  // }
  // FastLED.delay(30 / speed);
}

// demo of fast LED
void loopFastLED() {
  FastLED.clear();

  leds[index] = CRGB::White;
  leds[0] = CRGB::Black;
  FastLED.show();

  index++;
  if (index >= NUM_LEDS) {
    index = 0;
  }

  delay(30 / speed);
}
