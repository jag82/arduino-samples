# motion detector

Detect movement with the HC-SR501 sensor module. It looks like a half a golfball.  Be aware there are other modules you might be interested in using for motion detection.