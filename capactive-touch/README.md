# capactive touch

Using a piece of foil and some calculations from a library, you can tell if a person is touching a "button" and also how hard they're pressing. It uses a really high value resistor.  

It relies on the use of this library:  
https://playground.arduino.cc/Main/CapacitiveSensor/  


## quick start

Connect a wire from pin 2 (send), to a resistor (~ 1 MOhm), to a piece of tin foil taped to a desk.  Attach a second wire from the tin foil to pin 3 (receive).  


Install the library in Arduino IDE: `Tools > Manage Libraries > search for CapacitiveSensor v0.5.1`.  Run the code and touch the foil!  We output to the Serial Monitor (ctrl + shift + M) to see values.  

## notes
This is a really good rundown of uses for capactive touch, how to do it, and its gotchas.  
https://www.instructables.com/Capacitive-Sensing-for-Dummies/  