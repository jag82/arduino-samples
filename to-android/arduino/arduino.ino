int LED = 13;
int BUTTON = 2;
int DIAL = A0;

int buttonState = 0;
int prevButtonState = 0;

int dial;
bool isOn = false;

String command;

void setup() { 
	Serial.begin(115200); 
  pinMode(LED, OUTPUT);
} 

void loop() { 
  // send
  dial = analogRead(DIAL);
  // prevButtonState = buttonState;
  // buttonState = digitalRead(BUTTON);

   if (dial > 700 && !isOn) {
     digitalWrite(LED, HIGH);
     Serial.println("<ON>");
     isOn = true;
   } else if (dial < 400 && isOn) {
     digitalWrite(LED, LOW);
     Serial.println("<off>");
     isOn = false;
   }

  // receive
  if (Serial.available()) {
    command = Serial.readStringUntil('\n');
    command.trim();
    if (command.equals("<enable>")) {
      digitalWrite(LED, HIGH);
    }
    else {
      digitalWrite(LED, LOW);
    }
  }
}
