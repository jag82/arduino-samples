# arduino to android
Two way communication between an Arduino microcontroller and an Android smartphone.  It consists of two parts:
- android1: an android app (written in Java, established to be working even in 2023)
- android2: an attempt to get the original code working in Kotlin
- arduino: an arduino sketch

## quick start


## links
https://www.allaboutcircuits.com/projects/communicate-with-your-arduino-through-android/  
https://github.com/felHR85/UsbSerial/releases  
https://hingxyu.medium.com/arduino-android-serial-communication-b72b124142fb  
https://www.theengineeringprojects.com/2015/10/usb-communication-between-android-and-arduino.html
https://www.makeuseof.com/tag/6-easy-ways-connect-arduino-android/  
https://wiki.seeedstudio.com/ODYSSEY-X86J4105-Firmata/#:~:text=Firmata%20is%20an%20intermediate%20protocol,with%20the%20support%20for%20Firmata.  


## TODO:
- send more sophisticated messages to arduino than just counting bytes
- correctly buffer messages coming from arduino and display them

https://forum.arduino.cc/t/serial-input-basics-updated/382007
