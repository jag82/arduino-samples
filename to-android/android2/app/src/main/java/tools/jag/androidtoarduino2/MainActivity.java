package tools.jag.androidtoarduino2;

import androidx.appcompat.app.AppCompatActivity;

import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.ServiceConnection;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.os.Message;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import java.lang.ref.WeakReference;

public class MainActivity extends AppCompatActivity {

    static final String PREFIX = "<";
    static final String SUFFIX = ">";

    private final BroadcastReceiver mUsbReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            switch (intent.getAction()) {
                case UsbService.ACTION_USB_PERMISSION_GRANTED: // USB PERMISSION GRANTED
                    Toast.makeText(context, "USB Ready", Toast.LENGTH_SHORT).show();
                    break;
                case UsbService.ACTION_USB_PERMISSION_NOT_GRANTED: // USB PERMISSION NOT GRANTED
                    Toast.makeText(context, "USB Permission not granted", Toast.LENGTH_SHORT).show();
                    break;
                case UsbService.ACTION_NO_USB: // NO USB CONNECTED
                    Toast.makeText(context, "No USB connected", Toast.LENGTH_SHORT).show();
                    break;
                case UsbService.ACTION_USB_DISCONNECTED: // USB DISCONNECTED
                    Toast.makeText(context, "USB disconnected", Toast.LENGTH_SHORT).show();
                    break;
                case UsbService.ACTION_USB_NOT_SUPPORTED: // USB NOT SUPPORTED
                    Toast.makeText(context, "USB device not supported", Toast.LENGTH_SHORT).show();
                    break;
            }
        }
    };
    private UsbService usbService;
    private EditText editText;
    private Button sendButton;
    private Button clearButton;
    private TextView textView;

    private MyHandler mHandler;
    private final ServiceConnection usbConnection = new ServiceConnection() {
        @Override
        public void onServiceConnected(ComponentName arg0, IBinder arg1) {
            usbService = ((UsbService.UsbBinder) arg1).getService();
            usbService.setHandler(mHandler);
        }

        @Override
        public void onServiceDisconnected(ComponentName arg0) {
            usbService = null;
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        editText = findViewById(R.id.editText);
        sendButton = findViewById(R.id.send);
        clearButton = findViewById(R.id.clear);
        textView = findViewById(R.id.textView);

        sendButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!editText.getText().toString().equals("")) {
                    String data = editText.getText().toString().trim();
                    if (usbService != null) { // if UsbService was correctly binded, Send data
                        /*
                            WRITE MESSAGES TO SERIAL PORT (e.g. Arduino)
                         */
                        if (data.contains(PREFIX) || data.contains(SUFFIX)) {
                            Toast.makeText(MainActivity.this, "message contains illegal characters: " + PREFIX + SUFFIX, Toast.LENGTH_SHORT).show();
                        } else {
                            String command = PREFIX + data + SUFFIX;
                            usbService.write(command.getBytes());
                            textView.append("\nSEND: " + command);
                        }
                    }
                }
            }
        });

        clearButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                editText.setText("");
                textView.setText("");
            }
        });

        mHandler = new MyHandler(this);

    }

    @Override
    public void onResume() {
        super.onResume();

        IntentFilter filter = new IntentFilter();
        filter.addAction(UsbService.ACTION_USB_PERMISSION_GRANTED);
        filter.addAction(UsbService.ACTION_NO_USB);
        filter.addAction(UsbService.ACTION_USB_DISCONNECTED);
        filter.addAction(UsbService.ACTION_USB_NOT_SUPPORTED);
        filter.addAction(UsbService.ACTION_USB_PERMISSION_NOT_GRANTED);
        registerReceiver(mUsbReceiver, filter);

        if (!UsbService.SERVICE_CONNECTED) {
            startService(new Intent(this, UsbService.class));
        }
        Intent bindingIntent = new Intent(this, UsbService.class);
        bindService(bindingIntent, usbConnection, Context.BIND_AUTO_CREATE);

    }

    @Override
    public void onPause() {
        super.onPause();
        unregisterReceiver(mUsbReceiver);
        unbindService(usbConnection);
    }

    /*
     * This handler will be passed to UsbService. Data received from serial port is displayed through this handler
     */
    private static class MyHandler extends Handler {
        private final WeakReference<MainActivity> mActivity;
        
        private String command = "";
        private boolean readyForNewCommand = true;
        private boolean addedToCommand = false;

        public MyHandler(MainActivity activity) {
            mActivity = new WeakReference<>(activity);
        }

        @Override
        public void handleMessage(Message msg) {
            switch (msg.what) {
                case UsbService.MESSAGE_FROM_SERIAL_PORT:
                    /*
                        READ MESSAGES FROM SERIAL PORT (e.g. Arduino)
                     */
                    String data = ((String)msg.obj).trim();
//                    mActivity.get().textView.append("\n" + data);
                    processDataIntoCommand(data);
                    break;
                case UsbService.CTS_CHANGE:
                    Toast.makeText(mActivity.get(), "CTS_CHANGE",Toast.LENGTH_LONG).show();
                    break;
                case UsbService.DSR_CHANGE:
                    Toast.makeText(mActivity.get(), "DSR_CHANGE",Toast.LENGTH_LONG).show();
                    break;
            }
        }

        private void processDataIntoCommand(String data) {
            if (data.length() == 0) {
                return;
            }
//            mActivity.get().textView.append("\nprocess: " + data + " (" + data.length() + ") ");
            if (readyForNewCommand) {
//                mActivity.get().textView.append("\nSTART?");
                // check for start
                int index = data.indexOf(PREFIX);
                if (index >= 0) {
//                    mActivity.get().textView.append("\nSTART!");
                    // 1. start of message
//                    mActivity.get().textView.append("\ncmd: " + command + " (" + command.length() + ") ");
                    readyForNewCommand = false;
                    if (data.length() > 1) {
                        processDataIntoCommand(data.substring(index + 1));
                    }
                }
            }
            else {
//                mActivity.get().textView.append("\nEND?");
                // check for end
                int index = data.indexOf(SUFFIX);
                if (index == 0) {
//                    mActivity.get().textView.append("\nEND!");
                    // 2. end of message
//                    mActivity.get().textView.append("\ncmd: " + command + " (" + command.length() + ") ");
                    readyForNewCommand = true;
                    executeCommand(command);
                    command = "";
                    if (data.length() > index + 1) {
                        processDataIntoCommand(data.substring(index + 1));
                    }
                }
                else if (index > 0) {
//                    mActivity.get().textView.append("\nEND!!");
                    // 3. content + end of message
                    command += data.substring(0, index);
//                    mActivity.get().textView.append("\ncmd: " + command + " (" + command.length() + ") ");
                    readyForNewCommand = true;
                    executeCommand(command);
                    command = "";
                    if (data.length() > index + 1) {
                        processDataIntoCommand(data.substring(index + 1));
                    }
                }
                else {
//                    mActivity.get().textView.append("\nmore...");
                    // 4. content
                    command += data;
//                    mActivity.get().textView.append("\ncmd: " + command + " (" + command.length() + ") ");
                }
            }
        }

        private void executeCommand(String command) {
            mActivity.get().textView.append("\nRECEIVE: " + command);
        }

    }

    private void log(String text) {
        Toast.makeText(MainActivity.this, text, Toast.LENGTH_SHORT).show();

    }
}