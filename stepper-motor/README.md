# stepper motor

A stepper motor is a simple, cheap, noisy motor that can move in discrete steps.

Our sketch contains a simple way to directly control the stepper motor by having it move a step in your desired direction (cw, ccw). It contains a commented out example of the Stepper library, which is a simple library to control the motor with less boiler plate.  Any serious use (e.g. more complicated controls, more motors) should use the AccelStepper(?) library, which is not in the example.  

Our initial stepper motor is the 28BYJ-48, which has 2048(!) steps and a very low speed (2ms minimum time between steps required = 500 steps per second max, which means ~ 4 seconds for a complete revolution!).  

The wiring is simple:  
<peripheral>  --- <arduino>
IN1, IN2, IN3, IN4  --- 9, 10, 11, 12  
GND, VCC --- GND, 5V  

Note that the controller is already wired to the stepper motor (5 grouped wires). 