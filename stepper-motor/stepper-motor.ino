// without a library we can demonstrate basic turning cw and ccw
#define STEPPER_PIN_1 9
#define STEPPER_PIN_2 10
#define STEPPER_PIN_3 11
#define STEPPER_PIN_4 12

int step_number = 0;
boolean step_direction = true;
int max_steps = 2048;
int current_steps = 0;

// realistically we will want to use a library though
//#include <Stepper.h>
//
//const int stepsPerRevolution = 2048; // 28byj-48 has 2048 steps
//Stepper myStepper(stepsPerRevolution, STEPPER_PIN_1, STEPPER_PIN_2, STEPPER_PIN_3, STEPPER_PIN_4);
//int stepCount = 0;

#include <AccelStepper.h>

AccelStepper stepper(AccelStepper::FULL4WIRE, STEPPER_PIN_1, STEPPER_PIN_2, STEPPER_PIN_3, STEPPER_PIN_4);

void setup() {
   Serial.begin(9600);

  pinMode(STEPPER_PIN_1, OUTPUT);
  pinMode(STEPPER_PIN_2, OUTPUT);
  pinMode(STEPPER_PIN_3, OUTPUT);
  pinMode(STEPPER_PIN_4, OUTPUT);

//    stepper.setMaxSpeed(100);
//  stepper.setAcceleration(20);
//  stepper.moveTo(1000);

}

void loop() {
  OneStep(step_direction);
  current_steps++;
  if (current_steps > max_steps) {
    current_steps = 0;
    step_direction = !step_direction;
  }
  delay(2);
 // read the sensor value:

//for (int i = 0; i < 2; i++) {
//  if (stepCount >= 1) {
////    break;    
//  }
//  myStepper.setSpeed(10);
//  myStepper.step(stepsPerRevolution);
//  stepCount++;
//  Serial.println("one rotation done");

//      if (stepper.distanceToGo() == 0)
//    {
//        // Random change to speed, position and acceleration
//        // Make sure we dont get 0 speed or accelerations
//        delay(1000);
//        stepper.moveTo(rand() % 200);
//        stepper.setMaxSpeed((rand() % 200) + 1);
//        stepper.setAcceleration((rand() % 200) + 1);
//    }
//    stepper.run();

//  if (stepper.distanceToGo() == 0) {
//      stepper.moveTo(-stepper.currentPosition());    
//  }
//    stepper.run();
  
  
//    int sensorReading = analogRead(A0);
//    int sensorReading = 500;
//    int motorSpeed = map(sensorReading, 0, 1023, 0, 100);
//
//    if (motorSpeed > 0) {
//      myStepper.setSpeed(motorSpeed);    
//      myStepper.step(stepsPerRevolution / 100);
//    }
}

void OneStep(bool clockwise) {
    switch (step_number) {
      case 0:
        digitalWrite(STEPPER_PIN_1, HIGH);
        digitalWrite(STEPPER_PIN_2, LOW);
        digitalWrite(STEPPER_PIN_3, LOW);
        digitalWrite(STEPPER_PIN_4, LOW);
      break;
      case 1:
        digitalWrite(STEPPER_PIN_1, LOW);
        digitalWrite(STEPPER_PIN_2, HIGH);
        digitalWrite(STEPPER_PIN_3, LOW);
        digitalWrite(STEPPER_PIN_4, LOW);
      break;
      case 2:
        digitalWrite(STEPPER_PIN_1, LOW);
        digitalWrite(STEPPER_PIN_2, LOW);
        digitalWrite(STEPPER_PIN_3, HIGH);
        digitalWrite(STEPPER_PIN_4, LOW);
      break;
      case 3:
        digitalWrite(STEPPER_PIN_1, LOW);
        digitalWrite(STEPPER_PIN_2, LOW);
        digitalWrite(STEPPER_PIN_3, LOW);
        digitalWrite(STEPPER_PIN_4, HIGH);
      break;    
    }
    if (clockwise) {
      step_number = (step_number - 1 + 4) % 4;
    } else {
      step_number = (step_number + 1) % 4;    
    }
    
}
