# potentiometer

A dial that controls how much current flows through. It is commonly used for things like volume control, light dimming, radio station, etc.

Typically they have a knob and 3 pins:

```
   (knob)

|    |    |

5v  read  GND
```
