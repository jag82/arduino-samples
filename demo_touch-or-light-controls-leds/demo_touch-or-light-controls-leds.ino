// Capactive Touch
#include <CapacitiveSensor.h>

#define COMMON_PIN      4    // The common 'send' pin for all keys
#define NUM_OF_SAMPLES  10   // Higher number means more delay but more consistent readings

CapacitiveSensor key = CapacitiveSensor(COMMON_PIN, 5);

const int CAP_MIN = 200;
const int CAP_MAX = 950;
const int CAP_STEPS = 12;
int CAP_THRESHOLDS[CAP_STEPS];
int capIntensity = 0;
int capValue = 0;


// Photoresistor
int PHOTO = A4;
const int PHOTO_MIN = 0;
const int PHOTO_MAX = 1000;
const int PHOTO_STEPS = 12;
int PHOTO_THRESHOLDS[PHOTO_STEPS];
int photoIntensity = 0;
int photoValue = 0;


// LEDs
#include "FastLED.h"
#if FASTLED_VERSION < 3001000
#error "Requires FastLED 3.1 or later; check github for latest code."
#endif

#define DATA_PIN    7
//#define CLK_PIN   4
#define LED_TYPE    WS2812
#define COLOR_ORDER GRB
#define NUM_LEDS    12
#define BRIGHTNESS  100 // 0 - 255

CRGB leds[NUM_LEDS];


// Serial Comms
int DEFAULT_LED = 13;
int BUTTON = 2;
int buttonState = 0;
int prevButtonState = 0;
String command;


void setup() {
	Serial.begin(9600);

	// Capactive Touch
  key.set_CS_AutocaL_Millis(0xFFFFFFFF);

	for (int i = 0; i < CAP_STEPS; i++) {
		CAP_THRESHOLDS[i] = CAP_MIN + ((float)i/(float)(CAP_STEPS-1)) * (CAP_MAX-CAP_MIN);
	}


	// Photoresistor
	pinMode(PHOTO, INPUT);

	for (int i = 0; i < PHOTO_STEPS; i++) {
		PHOTO_THRESHOLDS[i] = PHOTO_MIN + ((float)i/(float)(PHOTO_STEPS-1)) * (PHOTO_MAX-PHOTO_MIN);
	}
	

	// LEDs
  FastLED.addLeds<LED_TYPE,DATA_PIN,COLOR_ORDER>(leds, NUM_LEDS)
    .setCorrection(TypicalLEDStrip)
    .setDither(BRIGHTNESS < 255);

  FastLED.setBrightness(BRIGHTNESS);


	// Serial Comms
	Serial.setTimeout(100);  //default is 1000, which causes a noticeable delay when receiving data from Android (and computer?)
	pinMode(DEFAULT_LED, OUTPUT);
	pinMode(BUTTON, INPUT);
}

void loop() {
	// Capactive Touch
  capIntensity = 0;
  capValue = key.capacitiveSensor(NUM_OF_SAMPLES);

  for (int i = CAP_STEPS - 1; i >= 0; i--) {
    if (capValue > CAP_THRESHOLDS[i]) {
    	capIntensity = i+1;
      break;
    }
  }


  // Photoresistor
  photoIntensity = 0;
	photoValue = analogRead(PHOTO);

  for (int i = PHOTO_STEPS - 1; i >= 0; i--) {
    if (photoValue > PHOTO_THRESHOLDS[i]) {
    	photoIntensity = i+1;
      break;
    }
  }


  // LEDs
  FastLED.clear();
  for(int led = 0; led < photoIntensity; led++) { 
      leds[led] = CRGB::Blue; 
  }
  FastLED.show();  


	// Serial Comms
		// send
   if (buttonState == HIGH && buttonState != prevButtonState) {
      digitalWrite(DEFAULT_LED, HIGH);
     Serial.println("<on>");
   } else if (buttonState == LOW && buttonState != prevButtonState) {
      digitalWrite(DEFAULT_LED, LOW);
     Serial.println("<off>");
   }


  	// receive
	if (Serial.available()) {
		command = Serial.readStringUntil('\n');
		command.trim();
		if (command.equals("<enable>")) {
			digitalWrite(DEFAULT_LED, HIGH);
		}
		else {
			digitalWrite(DEFAULT_LED, LOW);
		}
	}

}