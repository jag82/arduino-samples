# midi  

This guide shows how to connect a MIDI controller/instrument to an Arudino.  

TODO: when needed, we can also demo how to send midi messages from an Arduino out (to a computer or MIDI device).  


Here's how to setup the MIDI circuit:  
https://www.youtube.com/watch?v=GxfHijjn0ZM&list=PL4_gPbvyebyH2xfPXePHtx8gK5zPBrVkg&index=3  

Here's how to respond to MIDI messages:  
https://www.youtube.com/watch?v=Twx0kzxXvp4&list=PL4_gPbvyebyH2xfPXePHtx8gK5zPBrVkg&index=4  

https://github.com/FortySevenEffects/arduino_midi_library  