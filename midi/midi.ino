// WARNING: disconnect that data pin (pin 0, also known as RX0) when uploading a sketch, or it will fail to upload!
// it is for receiving serial data, which means MIDI messages but also the code upload from Arduino IDE

#include <MIDI.h>
#include <Arduino.h>

#define LED 13

// Create and bind the MIDI interface to the default hardware Serial port
MIDI_CREATE_DEFAULT_INSTANCE();

void setup() {
    pinMode(LED, OUTPUT);

    MIDI.begin(MIDI_CHANNEL_OMNI);  // Listen to all incoming messages
    MIDI.setHandleNoteOn(noteOn);
    MIDI.setHandleNoteOff(noteOff);
}

void loop() {
    // Read incoming messages
    MIDI.read();
  // digitalWrite(LED, LOW);
    // Send note 42 with velocity 127 on channel 1
    MIDI.sendNoteOn(42, 127, 1);

}

void noteOn(byte channel, byte pitch, byte velocity) {
    digitalWrite(LED, HIGH);
}

void noteOff(byte channel, byte pitch, byte velocity) {
    digitalWrite(LED, LOW);
}
