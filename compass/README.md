# compass

The HMC5883L is no longer being manufactured, and many of the "new" modules described as HMC5883L are actually a Chinese clone, QMC5883. 

The QMC5883 has different markings, different I2C address and requires a different library. Here is one of several such libraries.  

https://github.com/keepworking/Mecha_QMC5883L  


```
2017 Update

I have been battling with this GY-271 problem for many hours. I know this thread is very old, but it's prominent in the search results. Maybe my addendum can save other people hours of searching and frustration. I too were getting a constant, occasionally glitchy, unresponsive output and thought I'd bricked the board even though the I2C scanner "saw" it.

The GY-271 should have the HMC5883L chip, which nearly all of the libraries and example code you're likely to find on google/arduino rely on.  Some, presumably "fake," boards don't, they have the QMC5883L. Note Q, not H. The I2C register addresses are not the same.

The HMC is made by Honeywell, and the QMC is a Chinese version. (source: http://wiki.epalsite.com/index.php?title=HMC5883L(Or_QMC5883L)_Electronic_Compass   )

If you run the I2C scanner -> https://playground.arduino.cc/Main/I2cScanner

and it finds the address 0x0D then you have one of these QMC chips.

More info on the register addresses is here in the datasheet -> https://github.com/luckypm/commn-informations/blob/master/%E5%9C%B0%E7%A3%81%E6%96%87%E6%A1%A3/%E8%88%AA%E7%BA%AC5983%E6%9B%BF%E4%BB%A3%E6%96%99%E8%B5%84%E6%96%99/QMC5883L%20Datasheet%201.0%20.pdf


You could use the register info in that datasheet to update one of the preexisting libraries from adafruit, or the vanilla HMC5883L library, or you could do what I did, and find...
 
...DFrobot. My unending gratitude goes to dfrobot.com for creating a library that works with the QMC5883L. It took me far too long to find it, hence this post, but it has saved me from a very unhappy & vexing evening :)
 https://github.com/DFRobot/DFRobot_QMC5883
 ```
 
## gy-271
Using the GY-271 chip, you can create a compass. It is based on the HMC883L/QMC883L chip which has loads of sample code on the internet.  

Data sheet:  
https://cdn-shop.adafruit.com/datasheets/HMC5883L_3-Axis_Digital_Compass_IC.pdf  

Wiring and libraries:  
https://learn.adafruit.com/adafruit-hmc5883l-breakout-triple-axis-magnetometer-compass-sensor/wiring-and-test  

How to make a compass with a ring of lights:  
https://www.brainy-bits.com/find-your-way-using-the-hmc5883l/
https://www.youtube.com/watch?v=w3Gz472O93s  
The compass module uses A4 and A5 (why?)  (sda=data, scl=clock)

https://www.magnetic-declination.com/  
For Berlin:  
Magnetic Declination: +4° 18'  
Declination is POSITIVE (EAST)  

https://github.com/jarzebski/Arduino-HMC5883L  
HMC5883L_calibrate  

## gy-521
There is also the GY-521 which is a breakout board for the MPU-6050. Which contains an accelerometer, gyroscope, and thermometer. It can be used in conjuction with the HMC883L to create a tilt-compensated compass.


## links
Someone investigating how to calibrate compasses thoroughly
https://thecavepearlproject.org/2015/05/22/calibrating-any-compass-or-accelerometer-for-arduino/
https://plotly.com/

Again the place to start reading about IMU’s is probably the CHrobotics library. And I’ve heard rumors that the MPU6050 with the i2cdevlib DMP example sketch generates both quaternions and sensor-fused motion data at ~100Hz, so that might be a good code reference…
http://www.chrobotics.com/library
(there is a good section about quaternions there)

I am using Fabio Varesano's (may his soul rest in peace) calibration tool which is in Python and uses an ellipsoid into sphere algorithm.
http://www.varesano.net/blog/fabio/ellipsoid-sphere-optimization-using-numpy-and-linalg

gimbal rig
https://forum.arduino.cc/index.php?topic=145781.0

Making a compass via GPS (+ magnetometer)
https://www.youtube.com/watch?v=Dt4D-8VNH94

MPU6050
https://www.youtube.com/watch?v=wTfSfhjhAU0
https://github.com/jrowberg/i2cdevlib


MPU6050 + servos = head tracking
https://www.instructables.com/id/Gyroscopic-Arduino-Head-Tracking-Unit/


Magnetic measurement visualizer:
https://diydrones.com/profiles/blogs/advanced-hard-and-soft-iron-magnetometer-calibration-for-dummies?id=705844%3ABlogPost%3A1676387&page=2

Head tracking
http://www.edtracker.org.uk/
Head tracking (IR)
https://www.youtube.com/watch?v=M_VN4mMA6Cw

SensorFusion (~ 100 euros): Detailed info about accelerometers, gyroscopes, etc.
https://www.youtube.com/watch?v=C7JQ7Rpwn2k
https://invensense.tdk.com/wp-content/uploads/2017/11/SmartMotion_IntroTraining_customer_ver3.pdf

A very accurate (+ complex) compass:
http://sailboatinstruments.blogspot.com/2011/10/hi-resolution-custom-compass.html

Use MPU-6050 to make 3d audio:
https://github.com/trsonic/nvsonic-head-tracker

Consider head tracking in a fixed room (e.g. hacking nintendo wii)


Magnetic Calibration
https://www.hindawi.com/journals/js/2010/967245/

MPU-6050 vs -9150,-9250
https://sureshjoshi.com/embedded/invensense-imus-what-to-know/#:~:text=Invensense%20reply&text=The%20MPU%2D9150%20contains%20the,you%20see%20with%206050%20v.&text=The%20magnetometer%20on%20the%20MPU,little%20better%20across%20the%20board.

BNO-055?  use acc/gyro for pitch + roll, but yaw needs mag (simple trig after knowing magnetic north)
https://www.youtube.com/watch?v=p_qZ-ie2R7o
https://www.youtube.com/watch?v=2AO_Gmh5K3Q&list=PLGs0VKk2DiYwEo-k0mjIkWXlkrJWAU4L9

Tilt Compass (with MPU6050 + HMC883L)
https://www.youtube.com/watch?v=PY_17f5RByI
https://www.youtube.com/watch?v=spmMMIMboPY
https://www.youtube.com/watch?v=VldnoakNfk4

Digilent Pmod NAV VS MPU9250+BMP180
https://www.youtube.com/watch?v=jIuzImnrsHU


Develop ESPrtk?