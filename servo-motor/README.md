# servo motor

A motor that can be electrically controlled via code. 

Consider stepper vs servo motors:  
Which is more accurate stepper or servo?
To summarize, stepper motors are good solutions for applications with low speed, low acceleration, and low accuracy requirements. ... Servo motors are a better choice for systems requiring high speed, high acceleration, and high accuracy. The trade-off is a higher cost and complexity.

Servo motors know what position their shaft is in!  

We have a simple SG90 from Tower Pro to experiment with.  It has 3 wires:  
- red: battery positive (to 5V)
- brown: battery negative (to GND)
- orange: signal (make sure to use a PWN pin! we use pin 3 in our sketch)

WARNING: other brands/models will have different color codes!  

It's speed is 0,1 seconds (per 60degrees of rotation, the standard way speed is reported for servos).  This means a half revolution (it's full range of motion) takes 0,3 seconds.  This is quite a bit faster than our cheap stepper motor (~ 2 seconds for a half revolution).  

It provides 2,5 kg/cm of torque.  
