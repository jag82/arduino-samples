 #include <Servo.h>
 
Servo servoblau; 

int SERVO = 3;  // WARNING: must be PWN pin!

void setup() {
  servoblau.attach(SERVO); 
}

 

void loop() {
  // go to position 0 (this will be the same regardless of what the "starting" position of the shaft is)
  servoblau.write(0); 
  delay(1000);

  // go to 90deg CCW
  servoblau.write(90);
  delay(1000);

  // go to 180deg
  servoblau.write(180);
  delay(1000);


  // WARNING: the range of the SG90 motor is 180 degrees!
  servoblau.write(179);
  delay(1000);
}
