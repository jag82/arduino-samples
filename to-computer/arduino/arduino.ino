const int LED =  13; // 13 is the built-in led
const int BUTTON_PIN = 2;

// for reading messages from serial port (from a computer)
// const byte DATA_MAX_SIZE = 32;
// char data[DATA_MAX_SIZE];   // an array to store the received data

int buttonState = 0;
int prevButtonState = 0;

String command;

void setup() {
  pinMode(LED, OUTPUT);
  pinMode(BUTTON_PIN, INPUT);

  Serial.begin(9600);

}

void loop() {
  // send
  prevButtonState = buttonState;
  buttonState = digitalRead(BUTTON_PIN);

   if (buttonState == HIGH && buttonState != prevButtonState) {
     digitalWrite(LED, HIGH);
     Serial.println("ON");
   } else if (buttonState == LOW && buttonState != prevButtonState) {
     digitalWrite(LED, LOW);
     Serial.println("off");
   }

  // received
  if (Serial.available()) {
    command = Serial.readStringUntil('\n');
    command.trim();
    if (command.equals("enable")) {
      digitalWrite(LED, HIGH);
    }
    else {
      digitalWrite(LED, LOW);
    }
  }
}


// for reading data from a computer via serial port
// void receiveData() {
//   static char endMarker = '\n'; // message separator
//   char receivedChar;     // read char from serial port
//   int ndx = 0;          // current index of data buffer  // clean data buffer
//   memset(data, 32, sizeof(data));  // read while we have data available and we are
//   // still receiving the same message.
//   while(Serial.available() > 0) {    receivedChar = Serial.read();    if (receivedChar == endMarker) {
//       data[ndx] = '\0'; // end current message
//       return;
//     }    // looks like a valid message char, so append it and
//     // increment our index
//     data[ndx] = receivedChar;
//     ndx++;    // if the message is larger than our max size then
//     // stop receiving and clear the data buffer. this will
//     // most likely cause the next part of the message
//     // to be truncated as well, but hopefully when you
//     // parse the message, you'll be able to tell that it's
//     // not a valid message.
//     if (ndx >= DATA_MAX_SIZE) {
//       break;
//     }
//   }  // no more available bytes to read from serial and we
//   // did not receive the separato. it's an incomplete message!
//   Serial.println("error: incomplete message");
//   Serial.println(data);
//   memset(data, 32, sizeof(data));
// }
