const SerialPort = require('serialport');
const Readline = SerialPort.parsers.Readline;

const mpv = require('node-mpv');
const mpvPlayer = new mpv({ debug: false }, []);
const path = require('path')

// WARNING: node-mpv doesn't work on mac, so we can test this with an exec call instead
// const { exec } = require("child_process");

// to get the serial port, open the Arduino IDE > Tools > Port
// WARNING: you should quit the Arduino IDE before running this nodejs code, or the port will be busy!
var port = new SerialPort("/dev/cu.usbmodem14101", { baudRate: 9600 });

const parser = port.pipe(new Readline({ delimiter: '\n' }));// Read the port data
port.on("open", () => {
    console.log('serial port open');
});

parser.on('data', (data) => {
    console.log('got word from arduino:', data);

    if (data.startsWith('ON')) {
        console.log('onning');
        mpvPlayer.play();
        send('enable');
        // exec('mpv watch2.mp3')
    } else if (data.startsWith('off')) {
        console.log('offing');
        mpvPlayer.pause();
        send('disable');
    }
});

// mpvPlayer.load(path.join('.', 'watch.mp3'), 'append-play');

mpvPlayer.load(path.join('.', 'watch2.mp3'), 'append');
mpvPlayer.pause();
mpvPlayer.loop();

// mpvPlayer.play();

// process.on('SIGINT', function() {
//     console.log("Caught interrupt signal");

//     mpvPlayer.stop();
//     process.exit();
// });

// TODO: this chunks up, check the Arduino code
// let i = 10;
function send(command) {
    port.write(`${command}\n`);
}

// setInterval(sendData, 2000);
