# arduino to computer
Communicate between an Arduino and a computer using the serial port (via the default usb cable).  

## quick start
Wire up a button and read from pin 2, then upload `arduino.ino` to your Arduino.  It should turn the built-in light on when pressed. Make a note of the serial port address (Arduino IDE > Tool > Port > /dev/tty...).  

Update the serial port address in `main.js`.  Run `node main.js`.  You should see console output indicated the serial port has opened.  Press and hold the button to play the included mp3.  Releasing the button will pause the mp3.  

You can now also send signals from the nodejs app to the arduino (https://www.youtube.com/watch?v=utnPvE8hN3o). Currently, the button press from arduino will talk to the nodejs and cause the nodejs app to write to the arduino's serial port "enable", which will turn on the LED. This is a strange example, but it illustrates how this can be easily done. Note that this is a blocking call. To implement non-blocking reads, investigate: https://forum.arduino.cc/t/serial-input-basics-updated/382007  

## TODO:
- non blocking reads (see above)
- try https://github.com/firmata/firmata.js for more complex serial communication  

https://medium.com/@pkl9231/serial-communication-between-node-js-and-arduno-read-and-write-data-2a712e07d337  