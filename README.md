# arduino samples
This repo contains a 'hello world' program for many components and common setups. This should help you get the rust off when revisiting Arduino after a long break.

## ide
Arduino has its own IDE, but it's trash. You're much better off taking your editor of choice (Sublime Text?) and using it to control the Arduino IDE. 

Open your sketch in the Arduino IDE > Preferences > Use External Editor. This will grey out the Arduino IDE. You can now open up the same file in another editor (Sublime Text) and work from there. To build/run, return to Arduino IDE.

For syntax highlighting:  
View > Syntax > Open all with current extension as... > C++.

For error highlighting:  
- install SublimeLinter via PackageManager
- install SublimeLinter-cpplint
- install cpplint via commandline: `pip install cpplint`

For auto-formatting (cmd + shift + H by default):  
In theory this should work (using the same rules as the linter) https://packagecontrol.io/packages/ESLint-Formatter  
but, I haven't gotten it working, so you can try the SublimeAStyleFormatter package instead (`ctrl+alt+F` by default).

Note that it will work on `.cpp` files. To get it to run on `.ino` files:
Sublime Text > Preferences > Package Settings > SublimeLinter >
```
    "linters": {
        "cpplint": {
            "args": ["--extensions=ino"]
            "linelength": 400,
            "filter": "-legal/copyright"
        }
    }
```

You may want to also setup `eslint_d` if linting is slow on your system.  


## libraries
Traditionally you can use the Arduino IDE Library Manager (`ctrl+shift+I`) to find and install dependencies. As a real programmer, you're going to want to be able to add custom dependencies (arbitrary c++ code) too. To do this, you need to create a folder in the Arduino libraries folder (`~/Documents/Arduino/libraries`) with the name of your library and add the `.cpp` and `.h` files. See `building + running` for an example `Makefile` which can make referencing libraries more configurable.  

## building + running
You can use the Arduino IDE, but the way it handles libraries is restrictive. There will be other limitations you likely encounter as well, making it harder to save all the code in one folder and run it from there without manual setup.  
https://hackaday.com/2015/10/01/arduino-development-theres-a-makefile-for-that/. 

## hardware
Try to include wiring diagrams wherever possible to make debugging + replication easier!  

Resistors can be hard to read.  See this links for a good lesson https://www.arrow.com/en/research-and-events/articles/resistor-color-code#:~:text=Always%20read%20resistors%20from%20left,resistor%20from%20left%20to%20right.  See `how-to-read-resistors.jpg` for the copied summary.  You can also measure resistors to confirm their value (or if you're having trouble telling which direction to read it from) with a multimeter (https://learn.sparkfun.com/tutorials/how-to-use-a-multimeter/measuring-resistance#:~:text=Pick%20out%20a%20random%20resistor,or%20the%20actual%20resistor%20value.).  Select a scale and you'll get 0 or 1 if it's too high or low, or you'll get the resistor value (watch out for decimal places!).  See `how-to-measure-resistors.jpg`.  


getting shorter jumper cables so you don't keep building rat's nests!  (get a spool of 22awg / 0.64mm solid core wire and cut/strip it as needed)
https://www.allaboutcircuits.com/news/the-best-wire-for-breadboarding/  


Capacitors look like coke cans or little ceramic circles.  They act like really low capacity batteries.  Here's a guide to identifying them:  
https://learn.sparkfun.com/tutorials/capacitor-kit-identification-guide/all  
https://electronics.stackexchange.com/questions/18102/ceramic-capacitors-how-to-read-2-digit-markings  

Combining resistors + capactiors (when you don't have the correct value you need, you can use multiple lower values ones):  
https://learn.sparkfun.com/tutorials/capacitors/capacitors-in-seriesparallel#:~:text=Much%20like%20resistors%2C%20multiple%20capacitors,completely%20the%20opposite%20of%20resistors  

E.g. You can put two 100 Ohm resistors in series to get a200 Ohm resistor.  
`--[100 Ohm]--[100 Ohm]--`  

E.g. You can put two 100uF capacitors in **parallel** to get a 200uF capacitor.  
https://www.quora.com/Why-does-capacitance-decrease-in-series-connection#:~:text=The%20impedance%20of%20two%20capacitors,gap%2C%20the%20smaller%20the%20capacitance.  
`
   ---[100 uF]---
--|              |-- 
   ---[100 uF]---
`  

## debugging
Serial.begin(9600)
Serial Plotter?

## gotchas
//WARNING: DO NOT USE PIN 0 and 1 (Tx/Rx) on ARDUINO UNO!!!!
//WARNING: DO NOT USE SINGLE QUOTES WHEN DEBUGGING WITH SERIAL.PRINT!!

If the Arduino IDE is not staring on osx, try deleting `~/Library/Arduino15/` and restarting the IDE.







## todo:
generic smoothing functions (consider other utils like easing functions too)  
https://docs.arduino.cc/built-in-examples/analog/Smoothing  

How to get good sound?  
Try LM386 (class AB amplifier)   
https://www.youtube.com/watch?v=4ObzEft2R_g&list=PLZxU0qq0aFxgF43WuzfGlUcvQ5iLTUm_M&index=21
Try PAM8403 (class D amplifier)  
> Waiting on 100uF capacitors, soldering tip

Drive exernally powered speakers? headphones
> use relays? probably should add a relay sample to the main code (mostly pictures?)
https://www.youtube.com/watch?v=58XWVDnB7Ss




Gps Module
> waiting on components (June 2020)



Servos + Spotlights
highly desirable!



Midi In/Out
control lights?
modify via computer in-between?



Wifi?



Radio?
https://www.youtube.com/watch?v=7OMaf4L4Z3s&list=PLZxU0qq0aFxgF43WuzfGlUcvQ5iLTUm_M&index=15
