# cylon

A FastLED demo for LED light strips that mimics a red light sweeping back and forth, like a cylon (from Battlestar Galactica).